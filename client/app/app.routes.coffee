myApp.config [
	'$routeProvider'
	($routeProvider) ->
		$routeProvider.when('/',
			templateUrl: 'components/gauge/gaugeView.html'
			controller: 'GaugeCtrl'
			controllerAs: 'gCtrl')
		.otherwise redirectTo: '/'
		return
]