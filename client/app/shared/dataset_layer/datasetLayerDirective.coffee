myApp.directive('gaugeDatasetLayer', ->
  {
    scope: datasources: '='
    templateUrl: 'components/dataset_layer/datasetLayerView.html'
    replace: true
    controller: 'DatasetLayerCtrl'
    controllerAs: 'dlCtrl'
    bindToController: true
  }
).controller 'DatasetLayerCtrl', ($scope) ->
	@layers = []

	layer = { 
		measure: ''
		dimension: ''
	}

	@layers.push layer

	@createLayer = ->
		newLayer = {
			measure: ''
			dimension: ''
		}
		@layers.push newLayer
		return

	@deleteLayer = (index) ->
		@layers.splice(index,1)
		return

	@resetMeasure = (index) ->
		@layers[index].measure = ''
		return

	@resetDimension = (index) ->
		@layers[index].dimension = ''
		return

	return