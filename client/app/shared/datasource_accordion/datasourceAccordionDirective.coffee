myApp.directive('gaugeDatasourceAccordion', ->
  {
    scope: datasources: '='
    templateUrl: 'components/datasource_accordion/datasourceAccordionView.html'
    replace: true
    controller: 'DatasourceAccordionCtrl'
    controllerAs: 'daCtrl'
    bindToController: true
  }
).controller 'DatasourceAccordionCtrl', ($scope) ->
	return