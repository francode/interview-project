myApp.factory 'datasourceServiceFactory', ($http) ->
	factory = {data: null}
	$http.get('assets/js/datasets.json').success (result) ->
		factory.data = result.datasources
		return
	return factory

