var myApp;

myApp = angular.module('gauge', ['ngRoute', 'ngAnimate', 'ngDragDrop', 'foundation']);

myApp.config([
  '$routeProvider', function($routeProvider) {
    $routeProvider.when('/', {
      templateUrl: 'components/gauge/gaugeView.html',
      controller: 'GaugeCtrl',
      controllerAs: 'gCtrl'
    }).otherwise({
      redirectTo: '/'
    });
  }
]);
