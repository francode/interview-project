myApp.controller('GaugeCtrl', function($scope, datasourceServiceFactory) {
  this.set = new Object();
  $scope.$watch((function() {
    return datasourceServiceFactory.data;
  }), function(data) {
    return $scope.gCtrl.set.datasources = data;
  });
});

myApp.directive('gaugeDatasetLayer', function() {
  return {
    scope: {
      datasources: '='
    },
    templateUrl: 'components/dataset_layer/datasetLayerView.html',
    replace: true,
    controller: 'DatasetLayerCtrl',
    controllerAs: 'dlCtrl',
    bindToController: true
  };
}).controller('DatasetLayerCtrl', function($scope) {
  var layer;
  this.layers = [];
  layer = {
    measure: '',
    dimension: ''
  };
  this.layers.push(layer);
  this.createLayer = function() {
    var newLayer;
    newLayer = {
      measure: '',
      dimension: ''
    };
    this.layers.push(newLayer);
  };
  this.deleteLayer = function(index) {
    this.layers.splice(index, 1);
  };
  this.resetMeasure = function(index) {
    this.layers[index].measure = '';
  };
  this.resetDimension = function(index) {
    this.layers[index].dimension = '';
  };
});

myApp.directive('gaugeDatasourceAccordion', function() {
  return {
    scope: {
      datasources: '='
    },
    templateUrl: 'components/datasource_accordion/datasourceAccordionView.html',
    replace: true,
    controller: 'DatasourceAccordionCtrl',
    controllerAs: 'daCtrl',
    bindToController: true
  };
}).controller('DatasourceAccordionCtrl', function($scope) {});

myApp.factory('datasourceServiceFactory', function($http) {
  var factory;
  factory = {
    data: null
  };
  $http.get('assets/js/datasets.json').success(function(result) {
    factory.data = result.datasources;
  });
  return factory;
});
