// 1. LIBRARIES
// - - - - - - - - - - - - - - -
var $        = require('gulp-load-plugins')();
var argv     = require('yargs').argv;
var gulp     = require('gulp');
var rimraf   = require('rimraf');
var router   = require('front-router');
var sequence = require('run-sequence');
var haml     = require('gulp-ruby-haml');
var coffee   = require('gulp-coffee');
var gutil    = require('gulp-util');

// Check for --production flag
var isProduction = !!(argv.production);

// 2. FILE PATHS
// - - - - - - - - - - - - - - -
var paths = {
  assets: [
    './client/**/*.*',
    './client/assets/js/**/*.js',
    '!./client/**/*.{scss,css,js,coffee,haml}'
  ],
  componentsCoffee:[
    './client/app/components/**/*.coffee',
    './client/app/shared/**/*.coffee'
  ],
  componentsHaml:[
    './client/app/components/**/*.haml',
    './client/app/shared/**/*.haml'
  ],
  sass: [
    'client/assets/css/*.scss'
  ],
  foundationJS: [
    'bower_components/jquery/dist/jquery.js',
    'bower_components/jquery-ui/jquery-ui.js',
    'bower_components/angular/angular.js',
    'bower_components/angular-route/angular-route.js',
    'bower_components/angular-animate/angular-animate.js',
    'bower_components/foundation-apps/js/vendor/**/*.js',
    'bower_components/foundation-apps/js/angular/**/*.js',
    'bower_components/angular-dragdrop/src/angular-dragdrop.js',
    '!bower_components/foundation-apps/js/angular/app.js'
  ],
  appConfig: [
    'client/app/*.coffee',
    'client/assets/js/*.coffee'
  ]
}


// 3. TASKS
// - - - - - - - - - - - - - - -

// Cleans the build folder
gulp.task('clean', function(cb) {
  rimraf('./build', cb);
});


// Copies all of the main components of the site
// pre-processes any of the .haml and .coffeee files found within the specified folders
gulp.task('copy', ['copy:index:haml', 'copy:components:haml', 'copy:components:coffee'], function() {
  return gulp.src(paths.assets, {
      base: './client/'
    })
    .pipe(gulp.dest('./build'));
});


// Copies all of the root level .haml files
gulp.task('copy:index:haml', function() {
  return gulp.src('./client/*.haml')
    .pipe(haml())
    .pipe(gulp.dest('./build'));
});


// Copies all of the .haml files within the  "~/components" and "~/shared" directories
gulp.task('copy:components:haml', function() {
  return gulp.src(paths.componentsHaml)
    .pipe(haml())
    .pipe(gulp.dest('./build/components'))
  ;
});


// Copies all of the .coffee files within the  "~/components" and "~/shared" directories
gulp.task('copy:components:coffee', function() {
  var uglify = $.if(isProduction, $.uglify().on('error', function (e) {
    console.log(e);
  }));

  return gulp.src(paths.componentsCoffee)
    .pipe(coffee({bare: true}).on('error', gutil.log))
    .pipe(uglify)
    .pipe($.concat('components.js'))
    .pipe(gulp.dest('./build/assets/js'))
  ;
});


// Copies all of foundations dpependencies
gulp.task('copy:foundation', function(cb) {
  var uglify = $.if(isProduction, $.uglify().on('error', function (e) {
    console.log(e);
  }));

  gulp.src('bower_components/foundation-apps/js/angular/components/**/*.html')
    .pipe($.ngHtml2js({
      prefix: 'components/',
      moduleName: 'foundation',
      declareModule: false
    }))
    .pipe(uglify)
    .pipe($.concat('templates.js'))
    .pipe(gulp.dest('./build/assets/js'))
  ;

  // Iconic SVG icons
  gulp.src('./bower_components/foundation-apps/iconic/**/*')
    .pipe(gulp.dest('./build/assets/img/iconic/'))
  ;

  cb();
});



// Copies and compiles all of the .scss files in the application
gulp.task('sass', function () {
  return gulp.src(paths.sass)
    .pipe($.sass({
      includePaths: 'bower_components/foundation-apps/scss',
      outputStyle: (isProduction ? 'compressed' : 'nested'),
      errLogToConsole: true
    }))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie 10']
    }))
    .pipe(gulp.dest('./build/assets/css/'))
  ;
});


// Runs the javascript concatenation and minification tasks
gulp.task('uglify', ['uglify:foundation', 'uglify:app'])


// Runs the Javascript concat and minification for all of the Foundation dependencies
gulp.task('uglify:foundation',  function(cb) {
  var uglify = $.if(isProduction, $.uglify().on('error', function (e) {
    console.log(e);
  }));

  return gulp.src(paths.foundationJS)
    .pipe(uglify)
    .pipe($.concat('foundation.js'))
    .pipe(gulp.dest('./build/assets/js/'))
  ;
});


// Runs the Javascript concat and minification for the application javascipt files
gulp.task('uglify:app', function() {
  var uglify = $.if(isProduction, $.uglify().on('error', function (e) {
    console.log(e);
  }));

  return gulp.src(paths.appConfig)
    .pipe(coffee({bare: true}).on('error', gutil.log))
    .pipe(uglify)
    .pipe($.concat('app.js'))
    .pipe(gulp.dest('./build/assets/js/'))
  ;
});


// Runs a local server using the "/build" folder running on localhost:8080
gulp.task('server', ['build'], function() {
  return gulp.src('./build')
    .pipe($.webserver({
      port: 8080,
      host: 'localhost',
      fallback: 'index.html',
      livereload: true,
      open: true
    }))
  ;
});


// Builds the entire application into the "./build" folder
gulp.task('build', function(cb) {
  return sequence('clean', ['copy', 'copy:foundation', 'sass', 'uglify'], cb);
});


// Watches for any changes in the "./client" folder and runs the 
// respective tasks to process them and serve them to the live server
gulp.task('default', ['server'], function () {
  // Watch Sass
  gulp.watch(['./client/assets/css/**/*.scss'], ['sass']);

  // Watch JavaScript
  gulp.watch(['./client/app/*.coffee'], ['uglify:app']);
  gulp.watch(['./client/app/components/**/*.coffee', './client/app/shared/**/*.coffee'], ['copy:components:coffee']);

  // Watch static files
  gulp.watch(paths.assets, ['copy']);

  // Watch app templates
  gulp.watch(paths.componentsHaml, ['copy:components:haml']);
  gulp.watch(['./client/*.haml'], ['copy:index:haml']);
});
