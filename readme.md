# Interview Project

**Author:** *Francisco Marcano*

**Build Process:**

- NodeJS with npm for automated build process
- Bower for library dependencies and updates

**Technology Stack:**
  
  - AngularJS
  - CoffeeScript
  - Haml
  - Sass
  - [Foundation](http://foundation.zurb.com/apps/docs/#!/installation) *(AngularJS)*

**Helper Libraries:**

  - [Angular Drag & Drop](http://codef0rmer.github.io/angular-dragdrop/#/)

This is a small skill showcase application for job interview purposes. 

## Requirements

Build a simple gauge builder that allows a user to do the following:

  - See datasets grouped by data source
  - Allows the user to see a list of measures and dimensions for each dataset.
  - User should be able to drag and drop a measure to select a measure
  - User should be able to drag and drop a dimension to select a dimension
  - Use a JavaScript framework (Preferably using Angular but could another one like backbone)
  - Use a preprocessor language for any custom styles you create, if any (LESS, SCSS, etc..)
  - Use HAML to create your HTML
  - Use CoffeeScript for all JavaScript you write

## Client vs Build Files

All of the already built files can be found within the **build** folder in the root. Running a local server within the **build** folder will allow to you to see the finished application.

All of the development files can be found within the **client** folder in the root. Modifying these files won't have any effect on the **build** folder until the application is re-built again.

## Replicate Development Environment

  - [Node.js](http://nodejs.org): Use the installer for your OS.
  - [Git](http://git-scm.com/downloads): Use the installer for your OS.
    - Windows users can also try [Git for Windows](http://git-for-windows.github.io/).
  - [Gulp](http://gulpjs.com/) and [Bower](http://bower.io): Run `npm install -g gulp bower`
    - Depending on how Node is configured on your machine, you may need to run `sudo npm install -g gulp bower` instead, if you get an error with the first command.
  - [Ruby](https://www.ruby-lang.org/en/documentation/installation/) Install ruby as one of the build processes uses the *haml* gem for pre-processing
    - Run `gem install haml` to install the *Haml* gem needed for the build process


Clone this repo into your machine

```bash
git clone https://bitbucket.org/francode/brightgauge-interview-project.git
```

Install the dependencies. If you're running Mac OS or Linux, you may need to run `sudo npm install` instead, depending on how your machine is configured.

Run this at the root of the cloned folder

```bash
npm install
bower install
```

Check the **gulpfile.js** in the root folder for build steps and details.
To start the application run the following in the root of the folder

```bash
gulp
```

This will compile the Haml, CoffeeScript, Sass and assemble your Angular app. **Now go to `localhost:8080` in your browser to see it in action.** When you change any file in the `client` folder, the appropriate Gulp task will run to build new files.

To run the compiling process once, without watching any files, use the `build` command.

```bash
gulp build
```